package ru.pavlovka.nc.homework1;

import ru.pavlovka.nc.homework1.books.Author;
import ru.pavlovka.nc.homework1.books.Book;
import ru.pavlovka.nc.homework1.employers.Employee;
import ru.pavlovka.nc.homework1.geometry.*;

public class Main {

    public static void main(String[] args) {

        print(new Circle());
        print(new Circle(10));
        print(new Circle(15, "green"));

        print(new Rectangle());
        print(new Rectangle(10, 15));

        print(new Employee(1, "Sasha", "Grey", 100));

        print(new Book("Song of Ice and Harry Potter", new Author[]{new Author("J.R.R. Martin", "wings-of-winter-will-never-finish@yandex.ru", 'm'), new Author("Joane Rowling", "mother-of-wizards@hogwarts.uk", 'f')}, 100));
        print(new Book("The Lord of the Rings", new Author[]{new Author("J.R.R. Tolkien", "valinor@midleearth.uk", 'm')}, 100, 1));

        print(new MyPoint(10, 5));

        print(new MyTriangle(0,0, 5,5, 10,0));
        print(new MyTriangle(-3,0,0,3,0,-3));
    }

    static void print(Object o){
        System.out.println(o);

        if(o instanceof Area) {
            System.out.println("Area: " + ((Area) o).getArea());
        }
        if(o instanceof Rectangle){
            System.out.println("Perimeter: "+((Rectangle)o).getPerimeter());
        }
        if(o instanceof Employee){
            printEmployee((Employee)o, 50);
        }
        if(o instanceof Book){
            System.out.println("Author names: "+((Book)o).getAuthorNames());
        }
        if(o instanceof MyPoint){
            printDistance((MyPoint)o, new MyPoint(10, 20));
        }
        if(o instanceof MyTriangle){
            System.out.println("Triangle type: " + ((MyTriangle)o).getType());;
        }
    }

    static void printEmployee(Employee employee, int percent){
        System.out.println("Full name: " + employee.getName());
        System.out.println("Annual salary: " + employee.getAnnualSalary());
        System.out.println("Salary will raise by " +percent);
        System.out.println("Raise salary: "+employee.raiseSalary(percent)+" "+employee);
    }

    static void printDistance(MyPoint point, MyPoint secondPoint){
        System.out.println("Distance (0, 0) "+point.distance());
        System.out.println("Distance "+secondPoint+" "+point.distance(secondPoint));
    }
}
