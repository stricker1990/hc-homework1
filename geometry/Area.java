package ru.pavlovka.nc.homework1.geometry;

public interface Area {
    double getArea();
}
