package ru.pavlovka.nc.homework1.geometry;

public interface Perimeter {
    double getPerimeter();
}
